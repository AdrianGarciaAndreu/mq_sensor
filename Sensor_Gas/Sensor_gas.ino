#include <M5Stack.h>

#define analog_pin 35

#define RL 5 //Resistencia de carga
#define RAL (9.83) //Resistencia en aire limpio

#define MUESTRAS_CALIBRACION 50

///// Datos ////////////
const int muestras_size = 200;

//Estuctura para objetos muestras.
 struct {
  double valores[muestras_size];
  double maximo;
  double minimo;
  double media;
} muestras;

//Estructura para objetos raw.
struct{
  double valores[muestras_size];
  double minimo;
  double maximo;
  double media;
} muestras_raw;

//double muestras[muestras_size]; //Buffer de datos (voltios)
//double muestras_raw[muestras_size]; //Buffer de datos (raw)

int indice = 0; // Indice actual del buffer circular


int contador = 0; //contador de iteraciones

//double minimo, maximo, media;
//double minimo_raw, maximo_raw, media_raw;


//Calibración //////////
double R0 = 10;

/*
//Valores para la curva de concentración de CH4 (Butano)
const float X0 = 200; const float Y0 = 3;
const float X1 = 10000; const float Y1 = 0.7;

const float punto0[] = { log10(X0), log10(Y0) };
const float punto1[] = { log10(X1), log10(Y1) };
// Calcular pendiente y coordenada abscisas
const float scope = (punto1[1] - punto0[1]) / (punto1[0] - punto0[0]);
const float coord = punto0[1] - punto0[0] * scope;
*/

///////// FUNCIONES //////////////////


////////////////////////////////////
/////// Cálculos y Lecturas ////////
////////////////////////////////////


/* añade un valor al buffer circular 
 * (si se llega al número máximo de muestras, se reiniciar el indice a la posición 0
 */
void appendBuffers(double valorRaw, double valor_voltios){
   muestras_raw.valores[indice] = valorRaw;
   muestras.valores[indice] = valor_voltios;
 
   indice++;
 
   if (indice >= muestras_size) indice = 0;
}


void calcularMaximo(){
  double c_val = muestras.valores[0]; 
  double c_val_raw = muestras_raw.valores[0];
  
  for (int i=0; i<muestras_size; i++){
    if (muestras.valores[i]>c_val){ c_val=muestras.valores[i]; }
    if (muestras_raw.valores[i]>c_val_raw){ c_val_raw=muestras_raw.valores[i]; }
  }

  muestras_raw.maximo = c_val_raw;
  muestras.maximo = c_val;
}

void calcularMinimo(){
  double c_val = muestras.valores[0];
  double c_val_raw = muestras_raw.valores[0];
  
  for (int i=0; i<muestras_size; i++){
    if (muestras.valores[i]<c_val){ c_val=muestras.valores[i]; }
    if (muestras_raw.valores[i]<c_val_raw){ c_val_raw=muestras_raw.valores[i]; }

  }
  muestras_raw.minimo = c_val_raw;
  muestras.minimo = c_val;
}

void calcularMedia(){
  double c_val = 0;
  double c_val_raw = 0;
  
  for(int i=0; i<muestras_size; i++){
    c_val = c_val + muestras.valores[i];
    c_val_raw = c_val_raw + muestras_raw.valores[i];
  }
  
  muestras_raw.media = c_val_raw / muestras_size;
  muestras.media = c_val / muestras_size;  
  
}


/*
 * Lee un dato del pin analógico
 */
uint16_t obtenerDatos(){
  return analogRead(analog_pin);
}




////////////////////////////////////
/////// Calibración ////////////////
////////////////////////////////////

//Calculo de la resistncia para la calibración
double calc_resistencia(int raw){
  if(raw>0){ return ( ((double)RL*(4094-raw)/raw)); }
  else{ return 0; }
}


//Función para calibrar los valores analógicos del sensor
double Calibrar(){ 
  
  double valorVoltios = ((1.5*obtenerDatos())/1680); // Valores en voltios
  double val=0;
  
  // Se toma el número de muestras determinado con una separación en el tiempo entre ellas y se van sumando.
  for (int i=0; i<MUESTRAS_CALIBRACION; i++) {
    val += calc_resistencia(obtenerDatos());

    Serial.println('.'); delay(500); 
  }
  
  val = val/MUESTRAS_CALIBRACION; //Se divide entre el número de muestras (MEDIA)
  val = val/RAL; //Se divide el valor entre la resistencia en aire
  
  return val;
}



////////////////////////////////////
/////// Representación de datos ////
////////////////////////////////////


/*
 * Muestra datos por la pantalla del M5 Stack
 * Tanto en RAW como en voltios
 */
void RepresentarM5(){
      
      //Se imprime por pantalla del M5
      M5.Lcd.clear(); 
      M5.Lcd.setCursor(0,0);

      //RAW
      M5.Lcd.setTextColor(0xffff);
      M5.Lcd.print("Minimo raw: ");M5.Lcd.print(muestras_raw.minimo);M5.Lcd.println("");
      M5.Lcd.print("Maximo raw: ");M5.Lcd.print(muestras_raw.maximo); M5.Lcd.println("");
      M5.Lcd.print("Media raw: "); M5.Lcd.print(muestras_raw.media); M5.Lcd.println("");
      M5.Lcd.println("");
      
      //Voltios 
      M5.Lcd.setTextColor(0xff00);
      M5.Lcd.print("Minimo voltios: ");M5.Lcd.print(muestras.minimo,2);M5.Lcd.println("");
      M5.Lcd.print("Maximo voltios: ");M5.Lcd.print(muestras.maximo,2); M5.Lcd.println("");
      M5.Lcd.print("Media voltios: "); M5.Lcd.print(muestras.media,2); M5.Lcd.println("");
}

/*
 * Muestra datos por el monitor Serie, en caso de que se introduzca el caracter 'a'
 * Tanto en RAW como en voltios
 */
void RepresentarSerial(){
      
    while(Serial.available()){
    if(Serial.read()=='a'){
      
        Serial.println("VALORES RAW");
        Serial.print("Minimo raw: ");Serial.print(muestras_raw.minimo);Serial.println("");
        Serial.print("Maximo raw: ");Serial.print(muestras_raw.maximo); Serial.println("");Serial.println("");
        Serial.print("Media raw: "); Serial.print(muestras_raw.media); Serial.println("");
        Serial.println(""); Serial.println("");
        
        Serial.println("VALORES VOLTIOS");
        Serial.print("Minimo voltios: ");Serial.print(muestras.minimo);Serial.println("");
        Serial.print("Maximo voltios: ");Serial.print(muestras.maximo); Serial.println(""); Serial.println("");
        Serial.print("Media voltios: "); Serial.print(muestras.media); Serial.println("");
        Serial.println(""); Serial.println("");
      }
      
    }
}



/*
 * Se obtienen muestras y se almacenan en los buffers, tanto en RAW como en voltios,
 * Cada 100 muestras, se muestran los datos por pantalla, y en caso de intorducir la letra 'a'
 * por el monitor serie, se muestran por este los últimos datos maximos, minimos y media almacenados
 */
void capturarMuestras(){
    
    uint16_t valor = obtenerDatos();
    double valor_voltios = (1.5*valor)/1680;
    
    //Se añaden muestras al buffer
    appendBuffers(valor, valor_voltios);
    
    //Se cuenta hasta llegar a 100, en caso afirmativo se muestran los datos por pantalla
    if(contador==99){
      contador=0;

      //Calcular máximo, mínimo y media de las muestras obtenidas
      calcularMaximo();
      calcularMinimo();
      calcularMedia();

      RepresentarM5();
      
    }else {contador++;}


    RepresentarSerial(); 
  
 }






////////////////////////////////////
/////// Configuración //////////////
////////////////////////////////////

void setup() {
  dacWrite(25,0);

  //Se inicia el M5Stack
  M5.begin(); M5.Power.begin(); 
  M5.Lcd.setTextSize(2);
  
  //Inicio de la comunicación serie
  Serial.begin(115200);
  pinMode(analog_pin, INPUT);

  //Calibración del sensor
  Serial.println("Calibrando sensor...."); M5.Lcd.println("Calibrando sensor....");
  R0 = Calibrar();
  
  Serial.println("Calibración realizada");
  Serial.print("Resistencia R0 "); Serial.print(R0,2); Serial.println(" Kilo ohms");

  M5.Lcd.clear(); M5.Lcd.setCursor(0,0);
    M5.Lcd.println("Calibración realizada");
  M5.Lcd.clear(); M5.Lcd.setCursor(0,0);
  
  
}


////////////////////////////////////
/////// Lazo ///////////////////////
////////////////////////////////////
void loop() {
  capturarMuestras();
  delay(10);
  
}
