#include <M5Stack.h>
#include "driver/uart.h"
#include "esp_intr_alloc.h"

//UART
#define TX0_pin  1
#define RX0_pin  3

#define UART_BUF_SIZE (4096)

#define UART_EMPTY_THRESH_DEFAULT  (0)
#define UART_FULL_THRESH_DEFAULT  (1) // numero de bytes recibidos para generar la interrupcion
#define UART_TOUT_THRESH_DEFAULT   (0)


//handler para los tipos de interrupciones de la UART
static intr_handle_t handle_console;

//Timer 0
hw_timer_t *timer0;


#define analog_pin 35

#define RL 5 //Resistencia de carga
#define RAL (9.83) //Resistencia en aire limpio

#define MUESTRAS_CALIBRACION 50

int Flag_LCD=0;

///// Datos ////////////
const int muestras_size = 200;

const int char_buff_size = 6;
//Estuctura para objetos muestras.
 struct {
  double valores[muestras_size];
  double maximo;
  double minimo;
  double media;
  char buff_max[char_buff_size];
  char buff_min[char_buff_size];
  char buff_med[char_buff_size];
} muestras;

//Estructura para objetos raw.
struct{
  double valores[muestras_size];
  double minimo;
  double maximo;
  double media;
  char buff_max[char_buff_size];
  char buff_min[char_buff_size];
  char buff_med[char_buff_size];
} muestras_raw;


int indice = 0; // Indice actual del buffer circular
int contador = 0; //contador de iteraciones

//Calibración //////////
double R0 = 10;



/*==== INTERRUPCIONES ==== */


/*
 * ISR UART 0
 */

 
volatile uint8_t  rxbuf[UART_BUF_SIZE]; // Buffer de recepcion
volatile uint8_t Flag_uart_int = 0; // Flag para activar rutina asociada a la ISR

// Rutina de atencion a la interrupcion de la UART
static void  IRAM_ATTR ISR_UART(void *arg) {
  uint8_t rx_fifo_len;
  uint8_t i = 0;
  
  // Leer el numero de bytes del buffer de la UART
  rx_fifo_len = UART0.status.rxfifo_cnt; 
  
  // Leer los bytes del buffer
  while(rx_fifo_len){
   rxbuf[i++] = UART0.fifo.rw_byte; // read all bytes
   rx_fifo_len--;
   Flag_uart_int++;
  }

 // Limpiar el bit de status de la interrupcion de la UART
 uart_clear_intr_status(UART_NUM_0, UART_RXFIFO_FULL_INT_CLR);
 
}


/*
 * ISR Timer 0 
 */

volatile int Flag_ISR_Timer0 = 0;

void IRAM_ATTR ISR_Timer0(){
  Flag_ISR_Timer0 = 1;
}





///////// FUNCIONES //////////////////


////////////////////////////////////
/////// Cálculos y Lecturas ////////
////////////////////////////////////


/* añade un valor al buffer circular 
 * (si se llega al número máximo de muestras, se reiniciar el indice a la posición 0
 */
void appendBuffers(double valorRaw, double valor_voltios){
   muestras_raw.valores[indice] = valorRaw;
   muestras.valores[indice] = valor_voltios;
 
   indice++;
 
   if (indice >= muestras_size) indice = 0;
}


void calcularMaximo(){
  double c_val = muestras.valores[0]; 
  double c_val_raw = muestras_raw.valores[0];
  
  for (int i=0; i<muestras_size; i++){
    if (muestras.valores[i]>c_val){ c_val=muestras.valores[i]; }
    if (muestras_raw.valores[i]>c_val_raw){ c_val_raw=muestras_raw.valores[i]; }
  }

  muestras_raw.maximo = c_val_raw;
  muestras.maximo = c_val;
}

void calcularMinimo(){
  double c_val = muestras.valores[0];
  double c_val_raw = muestras_raw.valores[0];
  
  for (int i=0; i<muestras_size; i++){
    if (muestras.valores[i]<c_val){ c_val=muestras.valores[i]; }
    if (muestras_raw.valores[i]<c_val_raw){ c_val_raw=muestras_raw.valores[i]; }

  }
  muestras_raw.minimo = c_val_raw;
  muestras.minimo = c_val;
}

void calcularMedia(){
  double c_val = 0;
  double c_val_raw = 0;
  
  for(int i=0; i<muestras_size; i++){
    c_val = c_val + muestras.valores[i];
    c_val_raw = c_val_raw + muestras_raw.valores[i];
  }
  
  muestras_raw.media = c_val_raw / muestras_size;
  muestras.media = c_val / muestras_size;  
  
}


/*
 * Lee un dato del pin analógico
 */
uint16_t obtenerDatos(){
  return analogRead(analog_pin);
}




////////////////////////////////////
/////// Calibración ////////////////
////////////////////////////////////

//Calculo de la resistncia para la calibración
double calc_resistencia(int raw){
  if(raw>0){ return ( ((double)RL*(4094-raw)/raw)); }
  else{ return 0; }
}


//Función para calibrar los valores analógicos del sensor
double Calibrar(){ 
  
  double valorVoltios = ((1.5*obtenerDatos())/1680); // Valores en voltios
  double val=0;
  
  // Se toma el número de muestras determinado con una separación en el tiempo entre ellas y se van sumando.
  for (int i=0; i<MUESTRAS_CALIBRACION; i++) {
    val += calc_resistencia(obtenerDatos());

    uart_write_bytes(UART_NUM_0, ".\n", 2);
    delay(500); 
  }
  
  val = val/MUESTRAS_CALIBRACION; //Se divide entre el número de muestras (MEDIA)
  val = val/RAL; //Se divide el valor entre la resistencia en aire
  
  return val;
}



////////////////////////////////////
/////// Representación de datos ////
////////////////////////////////////

//Convierte un número flotante a un buffer de carácteres
void printNumberUART(char* buff, float var){
  snprintf(buff, 6, "%.2f", var); //Se escribe en la salida como un juego de caracteres  
}




/*
 * Muestra datos por la pantalla del M5 Stack
 * Tanto en RAW como en voltios
 */
void RepresentarM5(){
     
      //Se imprime por pantalla del M5
      M5.Lcd.clear(); 
      M5.Lcd.setCursor(0,0);

      //RAW
      M5.Lcd.setTextColor(0xffff);
      M5.Lcd.print("Minimo raw: ");M5.Lcd.print(muestras_raw.minimo);M5.Lcd.println("");
      M5.Lcd.print("Maximo raw: ");M5.Lcd.print(muestras_raw.maximo); M5.Lcd.println("");
      M5.Lcd.print("Media raw: "); M5.Lcd.print(muestras_raw.media); M5.Lcd.println("");
      M5.Lcd.println("");
      
      //Voltios 
      M5.Lcd.setTextColor(0xff00);
      M5.Lcd.print("Minimo voltios: ");M5.Lcd.print(muestras.minimo,2);M5.Lcd.println("");
      M5.Lcd.print("Maximo voltios: ");M5.Lcd.print(muestras.maximo,2); M5.Lcd.println("");
      M5.Lcd.print("Media voltios: "); M5.Lcd.print(muestras.media,2); M5.Lcd.println("");
}


/*
 * Muestra datos por el monitor Serie, en caso de que se introduzca el caracter 'a'
 * Tanto en RAW como en voltios
 */
void RepresentarSerial(){
      
         uart_flush(UART_NUM_0); //Se limpia el monitor serie primero

         //Si es una 'a'
         if(String((char*)rxbuf)=="a"){

          ///// RAW ///////
          uart_write_bytes(UART_NUM_0, "\n", 1);
          uart_write_bytes(UART_NUM_0, "RAW", sizeof("RAW")); uart_write_bytes(UART_NUM_0, "\n", 1);
          
          uart_write_bytes(UART_NUM_0, "Maximo: ", sizeof("Maximo: "));
          printNumberUART(muestras_raw.buff_max, muestras_raw.maximo);
          uart_write_bytes(UART_NUM_0, muestras_raw.buff_max, sizeof(muestras_raw.buff_max));
          
          uart_write_bytes(UART_NUM_0, " Minimo: ", sizeof(" Minimo: ")); 
          printNumberUART(muestras_raw.buff_min, muestras_raw.minimo);
          uart_write_bytes(UART_NUM_0, muestras_raw.buff_min, sizeof(muestras_raw.buff_min));
          
          uart_write_bytes(UART_NUM_0, " Media: ", sizeof(" Media: ")); 
          printNumberUART(muestras_raw.buff_med, muestras_raw.media);
          uart_write_bytes(UART_NUM_0, muestras_raw.buff_med, sizeof(muestras_raw.buff_med));
          
          uart_write_bytes(UART_NUM_0, "\n", 1);
          

          ///// VOLTIOS /////// 
          uart_write_bytes(UART_NUM_0, "VOLTIOS", sizeof("VOLTIOS")); uart_write_bytes(UART_NUM_0, "\n", 1);       
          uart_write_bytes(UART_NUM_0, "Maximo: ", sizeof("Maximo: "));
          printNumberUART(muestras.buff_max, muestras.maximo);
          uart_write_bytes(UART_NUM_0, muestras.buff_max, sizeof(muestras.buff_max));
          
          uart_write_bytes(UART_NUM_0, " Minimo: ", sizeof(" Minimo: ")); 
          printNumberUART(muestras.buff_min, muestras.minimo);
          uart_write_bytes(UART_NUM_0, muestras.buff_min, sizeof(muestras.buff_min));
          
          uart_write_bytes(UART_NUM_0, " Media: ", sizeof(" Media: ")); 
          printNumberUART(muestras.buff_med, muestras.media);
          uart_write_bytes(UART_NUM_0, muestras.buff_med, sizeof(muestras.buff_med));


          
          uart_write_bytes(UART_NUM_0, "\n", 1); uart_write_bytes(UART_NUM_0, "\n", 1);


         }
         
}



/*
 * Se obtienen muestras y se almacenan en los buffers, tanto en RAW como en voltios,
 * Cada 100 muestras, se muestran los datos por pantalla, y en caso de intorducir la letra 'a'
 * por el monitor serie, se muestran por este los últimos datos maximos, minimos y media almacenados
 */
void capturarMuestras(){
    
    uint16_t valor = obtenerDatos();
    double valor_voltios = (1.5*valor)/1680;
    
    //Se añaden muestras al buffer
    appendBuffers(valor, valor_voltios);
    
    //Se cuenta hasta llegar a 100, en caso afirmativo se muestran los datos por pantalla
    if(contador==99){
      contador=0; Flag_LCD=1;
      
      //Calcular máximo, mínimo y media de las muestras obtenidas
      calcularMaximo(); calcularMinimo(); calcularMedia();
    }else {contador++;}

  
 }






////////////////////////////////////
/////// Configuración //////////////
////////////////////////////////////

void setup() {
  dacWrite(25,0);

  //Se inicia el M5Stack
  M5.begin(); M5.Power.begin(); 
  M5.Lcd.setTextSize(2);
  pinMode(analog_pin, INPUT);

  Serial.end();

  //Se configura la UART
  uart_config_t uart_config = {
      .baud_rate = 115200,
      .data_bits = UART_DATA_8_BITS,
      .parity = UART_PARITY_DISABLE,
      .stop_bits = UART_STOP_BITS_1,
      .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
      .rx_flow_ctrl_thresh = 120,
      .use_ref_tick = false
  };

  //Se establece la configuración para la UART 0
  uart_param_config(UART_NUM_0, &uart_config);
  
  uart_set_pin(UART_NUM_0, TX0_pin, RX0_pin, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

  //Se instala el driver sobre la uart 0 con el tamaño de los buffer
  uart_driver_install(UART_NUM_0, UART_BUF_SIZE,0 ,0, NULL, 0);

  uart_write_bytes(UART_NUM_0, "Inicio de programa\n", 19);
  uart_write_bytes(UART_NUM_0, "Calibrando sensor...\n", 21);

  M5.Lcd.println("Inicio de programa");
  M5.Lcd.println("Calibrando sensor....");

  //Se calibra el sensor
  R0 = Calibrar();

  uart_write_bytes(UART_NUM_0, "Calibracion finalizada\n", 23);
  M5.Lcd.println("Calibracion finalizada");


  //Se configura el Timer
  timer0 = timerBegin(0,80,true);

  uart_isr_free(UART_NUM_0); //Se libera la interrupción de la UART_0

  //Se registra la interrupción para la UART 0
  uart_isr_register(UART_NUM_0,ISR_UART, NULL, ESP_INTR_FLAG_IRAM | ESP_INTR_FLAG_LEVEL1, &handle_console);


  // Configurar la interrupcion UART
    uart_intr_config_t uart0_intr;
    uart0_intr.intr_enable_mask = UART_RXFIFO_FULL_INT_ENA_M ;
    uart0_intr.rxfifo_full_thresh = UART_FULL_THRESH_DEFAULT;
    uart0_intr.rx_timeout_thresh = UART_TOUT_THRESH_DEFAULT;
   
    uart_intr_config(UART_NUM_0, &uart0_intr);

  // Habilitar la máscara de interrupciones
    uart_enable_intr_mask(UART_NUM_0, UART_RXFIFO_FULL_INT_ENA );


  //Interrupción timer
  timerAttachInterrupt(timer0, &ISR_Timer0, true); //Se registra la interrupción ISR_Timer0

  
  timerAlarmWrite(timer0, 10000, true); //Cada 10ms
  timerAlarmEnable(timer0); //Se habilita la alarma
  
    
  
}


////////////////////////////////////
/////// Lazo ///////////////////////
////////////////////////////////////
void loop() {
  
  timerStart(timer0); //Se inicia el timer

  //Flag del timer, cada 10ms
  if(Flag_ISR_Timer0>0){
      capturarMuestras(); //Se toman muestras, y se calculan máximos, mínimos y media
    Flag_ISR_Timer0 = 0;
  }

  //Flag LCD, cada 100 muestras, se muestran los valores por pantalla
  if(Flag_LCD>0){
      RepresentarM5();
      Flag_LCD=0;
    }

  //Flag UART, al introducir el carácter 'a', se muestran los datos por el monitor serie
  if(Flag_uart_int>0){
      RepresentarSerial();
    Flag_uart_int = 0;
   }

  
}
